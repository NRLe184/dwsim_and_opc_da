#1. Версия интерпретатора - 3.7.0, на других может не работать
#2. Версия библиотеки pythonnet - 2.x
#3. Исходные импорты лучше не трогать, может всё поломаться
#4. DWSIM желательно должна быть установлена в пути по умолчанию, настоятельно советуется при установке выбрать ВСЕ дополнительные пакеты функций

import clr
import sys
import pythoncom
import redis
from array import array
import time

pythoncom.CoInitialize()
from System.IO import Directory, Path, File
from System import String, Environment
from System import Array, Char
sys.path.append("C:\\Users\\Кирилл\Desktop\DWSIM_MODEL\dwsim_model\\venv")
dwSimPath = r"C:\Users\User\AppData\Local\DWSIM\\"
clr.AddReference(dwSimPath + "CapeOpen.dll")
clr.AddReference(dwSimPath + "DWSIM.Automation.dll")
clr.AddReference(dwSimPath + "DWSIM.Interfaces.dll")
clr.AddReference(dwSimPath + "DWSIM.GlobalSettings.dll")
clr.AddReference(dwSimPath + "DWSIM.SharedClasses.dll")
clr.AddReference(dwSimPath + "DWSIM.Thermodynamics.dll")
clr.AddReference(dwSimPath + "DWSIM.UnitOperations.dll")
clr.AddReference(dwSimPath + "DWSIM.Inspector.dll")
clr.AddReference(dwSimPath + "DWSIM.DynamicsManager.dll")
clr.AddReference(dwSimPath + "System.Buffers.dll")
clr.AddReference(dwSimPath + "DWSIM.FlowsheetSolver.dll")

from DWSIM.Interfaces.Enums.GraphicObjects import ObjectType
from DWSIM.Thermodynamics import Streams, PropertyPackages
from DWSIM.UnitOperations import UnitOperations
from DWSIM.Automation import Automation3
from DWSIM.GlobalSettings import Settings
from DWSIM.DynamicsManager import *
from DWSIM.FlowsheetSolver import *

# Решить что делать с единицами измерениями, через API не получается (по крайней мере сейчас)
def set_object_properties(object, object_type, object_name):
    current_object = r.hgetall(object_name)
    print(object_name, current_object)
    if object_type == "Material Stream":
        object.SetTemperature(round(float(current_object['Temperature']), 6))
        object.SetPressure(round(float(current_object['Pressure']), 6))
        object.SetMassFlow(round(float(current_object['MassFlow']), 6))
        object.SetMolarFlow(round(float(current_object['MolarFlow']), 6))
        object.SetVolumetricFlow(round(float(current_object['VolumetricFlow']), 6))
        object.SetMassEnthalpy(round(float(current_object['MassEnthalpy']), 6))
        object.SetMassEntropy(round(float(current_object['MassEntropy']), 6))
        #object.SetOverallComposition(Array[Char](array('i', [1.0])))



        #print(f"Энергия потока: {object.GetEnergyFlow()} kW")
        #print(f"Компоненты потока: {''.join(object.get_ComponentIds())}")
        #print(f"Массовая доля компонентов потока {object.GetOverallComposition()}")
        #print(f"Количество фаз потока: {object.GetNumPhases()}")
        return

    elif object_type == "Valve" or object_type == "Válvula":
        object.SetPropertyValue('PROP_VA_0', float(current_object['CalcMode'])) #PROP_VA_0 и аналогичные далее названия свойств взяты из источников кода в гитхабе
        #можно перейти в источники со страницы API-документации либо воспользоваться поиском по репозиторию
        object.SetPropertyValue('PROP_VA_1', float(current_object['DeltaP']))
        object.SetPropertyValue('PROP_VA_2', float(current_object['OutletPressure']))
        '''
        print(object.SetPropertyValue("OutletPressure", 90000))
        print(f"Выходная температура: {object.OutletTemperature} K")
        object_properties_dict = {'OutletPressure': object.OutletPressure, 'OutletTemperature': object.OutletTemperature}
        print('--------------------------------------')
        return object_properties_dict
        '''
        pass
    elif object_type == "Tank":
        object.SetPropertyValue('PROP_TK_1', float(current_object['Volume']))
        #print(f"Время пребывания жидкости: {object.ResidenceTime} s") # Время зависит от объема
        #object_properties_dict = {'Volume': object.Volume, 'ResidenceTime': object.ResidenceTime}
        #print('--------------------------------------')
        return
    elif object_type == "Level Gauge" or object_type == "Medidor de Nível": #в API не реализованы функции для установление объекту этого типа нового значения
        '''
        print(object.GetProperties(2))
        print(object.SetPropertyValue('PROP_LG_1', 1))
        #print(f"Минимальный объём: {object.MinimumValue}")
        #print(f"Максимальный объём: {object.MaximumValue}")
        #print(f"Текущий объём: {object.CurrentValue}")
        #print(f"Свойство: {object.SelectedProperty}")
        #object_properties_dict = {'MinimumValue': object.MinimumValue, 'MaximumValue': object.MaximumValue,
        #                          'CurrentValue': object.CurrentValue, 'SelectedProperty': object.SelectedProperty}
        #print('--------------------------------------')
        return #object_properties_dict
        '''
        pass
    elif object_type == "PID Controller" or object_type == "Controlador PID":
        object.SetPropertyValue('SetPointAbs', float(current_object['SetPoint']))
        object.SetPropertyValue('OutputMin', float(current_object['OutputMin']))
        object.SetPropertyValue('OutputMax', float(current_object['OutputMax']))
        object.SetPropertyValue('Kp', float(current_object['Kp']))
        object.SetPropertyValue('Ki', float(current_object['Ki']))
        object.SetPropertyValue('Kd', float(current_object['Kd']))
        return
    return



r = redis.Redis(host='localhost', port=6379, decode_responses=True)
interf = Automation3()
fileNameToLoad = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), r"C:\Users\User\AppData\Local\DWSIM\samples\edited_samples\Water_Level_Tank_Control_Edited.dwxmz")
sim = interf.LoadFlowsheet(fileNameToLoad)
flowsheet_objects = sim.get_SimulationObjects()


try:
    for obj in flowsheet_objects:
        object_name = obj.get_Value().ToString()
        object_type = obj.get_Value().GetAsObject().GetDisplayName()
        try:
            object_name = object_name[:object_name.index(':')]
        except:
            pass
        current_object = sim.GetFlowsheetSimulationObject(object_name)
        set_object_properties(current_object, object_type, object_name)
    fileNameToSave = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), r"C:\Users\User\AppData\Local\DWSIM\samples\edited_samples\Water_Level_Tank_Control_Edited.dwxmz")
    interf.SaveFlowsheet(sim, fileNameToSave, True)
    print('Все параметры установлены в модель')
except Exception as ex:
    print(f"При внесении изменений в модель возникла ошибка: {ex}")
    print(f"Изменения не были внесены, модель осталась прежней")
time.sleep(2)
