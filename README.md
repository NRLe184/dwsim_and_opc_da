# dwsim_and_opc_da

Данный репозиторий содержит в себе два файла с расширением .py и один проект в Visual Studio 2022, подробнее о каждом из них ниже.

## 0. Документация к работе над проектом
Во время работы над проектом использовались следующие источники:

1. https://dwsim.org/api_help/html/G_DWSIM.htm – API к программе DWSIM(API-документация сгенерирована автоматически, поэтому нередко функции придётся смотреть в исходниках кода на VB и C#)
2. https://redis.io/docs/ (client quickstarts) – документация к облачной базе данных Redis
3. https://sourceforge.net/p/dwsim/discussion/ – форум по DWSIM, разработчик переодически отвечает на вопросы
4. https://github.com/virajdesai0309/DWSim-Automation-Repo/blob/main/README.md – примеры взаимодествия с моделью DWSIM через API
5. https://studfile.net/preview/3976641/page:2/ – создание тегов в MatrikonOpc Server for Simulation (OPC_DA сервер)
6. https://matrikonopc.com/index.aspx – сайт для загрузки MatrikonOpc Server for Simulation(потребуется регистрация)
7. https://dwsim.org/ – сайт DWSIM, откуда можно скачать программу

## 1. Сборка проекта

Файлы не объединены в одну единую экосистему, поэтому для редактирования кода на python и C# потребуется запускать файлы в разных средах разработки либо пользоваться расширениями в VS: Code. 

Разработка велась на языке программирования Python 3.7.0 и платформе .NET 6.0 в PyCharm/VS:Code и Visual Studio 2022

!!!Скрипты get_model_data.py и set_data_to_model.py написаны с упором на **КОНКРЕТНУЮ МОДЕЛЬ СИМУЛЯЦИИ**, она доступна по адресу:
`C:\Users\(ваше имя пользователя)\AppData\Local\DWSIM\samples\Dynamic Simulation - Water Tank Level Control.dwxmz`
!!!

Можно подгрузить другую модель, но в таком случае скрипты могут работать некорректно; чтобы расширить возможности скрипта по считыванию и установке данных в модель нужно реализовать получение(внесение) данных с объектов других типов, нереализованных внутри скриптов.

P.S. Разработка велась в отдельных репозиториях для каждой части проекта(Python-часть и C#), при необходимости ознакомиться с этими репозиториями - пишите мне на почту **kaus.kirill.fit.201@gmail.com**

## 2. get_model_data.py 

Данный скрипт выполняет задачу по загрузке модели, просчёту модели и чтению параметров из просчитанной модели

Версия интерпретатора - 3.7.0, на других может не работать

Библиотеки:

pythonnet - версии 2.x

pyinstaller - `pip3.7 install pyinstaller`

DWSIM желательно должна быть установлена в пути по умолчанию, который предлагает установщик (по умолчанию он закидывает программу в скрытую папку AppData по адресу C:\Users\User\AppData\Local\DWSIM) настоятельно советуется при установке выбрать ВСЕ дополнительные 
пакеты функций во избежание проблем с импортом.

**!!!Если команды import будут распознаваться средой разработки как ошибки(подчёркиваться) – это ни о чём не говорит, и нужно запускать код. Они могут спокойно отработать, если всё сделано правильно!!!**


```
import clr
import sys
import pythoncom
import redis
import time
```

Добавление в sys.path пути к виртуальному окружению для корректного импорта библиотек
```
sys.path.append("C:\\Users\\User\Desktop\DWSIM_MODEL\dwsim_model\\venv")
```


Указываем путь к папке установки DWSIM

`dwSimPath = r"C:\Users\User\AppData\Local\DWSIM\\"`

Добавляем ссылки на библиотеки DWSIM с помощью модуля clr
Метод AddReference используется для добавления ссылки на сборку .NET.
Путь к сборке строится с использованием переменной dwSimPath и имени файла сборки
Каждая сборка предоставляет определенный набор функций

Библиотеки написаны на .NET, поэтому возможны некоторые проблемы с подсказками во время использования функций, то есть они не будут выводиться, а при неверно ввёдённом аргументе среда разработки не выведет вам конкретную ошибку.
```
clr.AddReference(dwSimPath + "CapeOpen.dll")
clr.AddReference(dwSimPath + "DWSIM.Automation.dll")
clr.AddReference(dwSimPath + "DWSIM.Interfaces.dll")
clr.AddReference(dwSimPath + "DWSIM.GlobalSettings.dll")
clr.AddReference(dwSimPath + "DWSIM.SharedClasses.dll")
clr.AddReference(dwSimPath + "DWSIM.Thermodynamics.dll")
clr.AddReference(dwSimPath + "DWSIM.UnitOperations.dll")
clr.AddReference(dwSimPath + "DWSIM.Inspector.dll")
clr.AddReference(dwSimPath + "DWSIM.DynamicsManager.dll")
clr.AddReference(dwSimPath + "System.Buffers.dll")
clr.AddReference(dwSimPath + "DWSIM.FlowsheetSolver.dll")
```


Импорт определенных классов из библиотек DWSIM с использованием синтаксиса "from...import"
Классы используются в последующем коде для доступа к определенным функциям, предоставляемым DWSIM.


```
from DWSIM.Interfaces.Enums.GraphicObjects import ObjectType
from DWSIM.Thermodynamics import Streams, PropertyPackages
from DWSIM.UnitOperations import UnitOperations
from DWSIM.Automation import Automation, Automation2, Automation3
from DWSIM.GlobalSettings import Settings
from DWSIM.DynamicsManager import *
from DWSIM.FlowsheetSolver import FlowsheetSolver
```
Подключение к базе данных Redis

`r = redis.Redis(host='localhost', port=6379, decode_responses=True)`

Путь к файлу схемы создается с использованием метода Environment.GetFolderPath для получения пути к папке на рабочем столе и относительного пути к файлу схемы.
**По какой-то причине функция не кушает относительный путь к файлу, только абсолютный. Ошибка отсылает в исходники кода DWSIM**

`fileNameToLoad = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), r"C:\Users\User\AppData\Local\DWSIM\samples\Dynamic Simulation - Water Tank Level Control.dwxmz")`

Загрузка и запуск решения модели по статитке:

```
sim = interf.LoadFlowsheet(fileNameToLoad)
Settings.SolverMode = 0 #очень важный параметр, без него не будет осуществлено изменение параметров объектов схемы просчитанной модели
errors = interf.CalculateFlowsheet2(sim) #считается статическая схема
flowsheet_objects = sim.get_SimulationObjects() #объекты со схемы с новыми после просчёта свойствами
```

Сохранение модели(при конфликте имён в папке заменяет исходный файл)


```
fileNameToSave = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), r"C:\Users\User\AppData\Local\DWSIM\samples\Dynamic Simulation - Water Tank Level Control.dwxmz")
interf.SaveFlowsheet(sim, fileNameToSave, True)

time.sleep(2) #нужно для просмотра поведения скрипта при запуске его через клиент
```

Для доступа к скрипту из проекта C# надо его скомпилировать

`pyinstaller --onefile get_model_data.py`

Данная команда создаст папку dist с exe-файлом

## 3. set_data_to_model.py 

Данный скрипт выполняет задачу занесению параметров, считанных из базы данных redis в модель. 

Версия интерпретатора - 3.7.0, на других может не работать

Библиотеки:

pythonnet - версии 2.x

pyinstaller - версия под python 3.7 `pip3.7 install pyinstaller`

Подключение основных модулей и библиотек по аналогичному с get_model_data.py принципу

Подключение к Redis и загрузка модели

```
r = redis.Redis(host='localhost', port=6379, decode_responses=True)
interf = Automation3()
fileNameToLoad = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), r"C:\Users\User\AppData\Local\DWSIM\samples\Dynamic Simulation - Water Tank Level Control.dwxmz")
sim = interf.LoadFlowsheet(fileNameToLoad)
flowsheet_objects = sim.get_SimulationObjects()
```

За установку параметров объектов в модель отвечает функция 
`set_object_properties(current_object, object_type, object_name)`

В этой функции происходит обращение к объектам в модели посредством API. В коде этой функции встречаются закомментированные строки, потому что:

    1. Конфликт с типами данных: так как модули DWSIM написаны на VB и C#, то возникал конфликт при работе со списками, например, здесь:
```
    def set_object_properties(object, object_type, object_name):
        if object_type == "Material Stream":
            (...)
            #object.SetOverallComposition(Array[Char](array('i', [1.0])))
```

    2. Отсутствует реализация метода в API как таковая(функция возвращает только строку либо булевое значение, а само тело пустое)
    Например, как здесь 
```
        elif object_type == "Level Gauge" or object_type == "Medidor de Nível": #в API не реализованы функции для установление объекту этого типа нового значения
        '''
        print(object.GetProperties(2))
        print(object.SetPropertyValue('PROP_LG_1', 1))
        #print(f"Минимальный объём: {object.MinimumValue}")
        #print(f"Максимальный объём: {object.MaximumValue}")
        #print(f"Текущий объём: {object.CurrentValue}")
        #print(f"Свойство: {object.SelectedProperty}")
        #object_properties_dict = {'MinimumValue': object.MinimumValue, 'MaximumValue': object.MaximumValue,
        #                          'CurrentValue': object.CurrentValue, 'SelectedProperty': object.SelectedProperty}
        #print('--------------------------------------')
        return #object_properties_dict
        '''
```
    3. Данный параметр представлен только в формате ReadOnly либо рассчитывается на основе другого параметра:
```
    elif object_type == "Tank":
        object.SetPropertyValue('PROP_TK_1', float(current_object['Volume'])) 
        #print(f"Время пребывания жидкости: {object.ResidenceTime} s") # Время зависит от объема
        #object_properties_dict = {'Volume': object.Volume, 'ResidenceTime': object.ResidenceTime}
        #print('--------------------------------------')
        return
```

Внесение новых значений параметров в модель

```
try:
    for obj in flowsheet_objects:
        object_name = obj.get_Value().ToString()
        object_type = obj.get_Value().GetAsObject().GetDisplayName()
        try:
            object_name = object_name[:object_name.index(':')] 
        except:
            pass
        current_object = sim.GetFlowsheetSimulationObject(object_name)
        set_object_properties(current_object, object_type, object_name)
    fileNameToSave = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), r"C:\Users\User\AppData\Local\DWSIM\samples\Dynamic Simulation - Water Tank Level Control.dwxmz")
    interf.SaveFlowsheet(sim, fileNameToSave, True)
    print('Все параметры установлены в модель')
except Exception as ex:
    print(f"При внесении изменений в модель возникла ошибка: {ex}")
    print(f"Изменения не были внесены, модель осталась прежней")
time.sleep(2)
```

Поскольку при типе объекта "Material Stream" при попытке обратиться к его имени возвращает не только имя самого объекта, но
и его основные свойства, то необходима конструкция
```
        try:
            object_name = object_name[:object_name.index(':')] 
        except:
            pass
```

Для доступа к скрипту из проекта C# надо его также скомпилировать

`pyinstaller --onefile set_data_to_model.py`

# 4. OPC-DA.sln

Данный проект написан на платформе .NET версии 6.0 с использованием библиотеки OPCAutomation.

Для работы данного проекта необходим MatrikonOpc Server for Simulation

Для подключении библиотеки OPCAutomation:
 1. Установить MatrikonOPC for Simulation;
 2. Открыть проект, добавить ссылку на COM-объект, который называется "Matrikon OPC Automation 2.0".

Для подключения библиотеки базы данных Redis установить пакет NuGet, который называется "StackExchange.Redis".

## Структура проекта

Проект представляет собой WPF приложение, написанное на платформе .NET версии 6.0. Функционал данного приложения направлен на работу в качестве к OPC-DA серверу с возможностью чтения и редактирования данных в тегах (свойствах объектов модели DWSIM).

## Старт работы

1. Запустить MatrikonOpc Server for Simulation;
2. Добавить группы (объекты модели DWSIM) и в каждую группу добавить соответствующие теги (свойства объектов модели DWSIM), в папке репозитория (Flowsheets_from_DWSIM_on_OPC_Server) содержится готовый набор групп и тегов (TancControlDWSIM_example.xml) для тестовой модели DWSIM под названием Dynamic Simulation - Water Tank Level Control;
3. Выбрать конфигурацию x86 в настройках проекта (если выбрать другую, то при соединении будет выдавать ошибку);
4. Запустить OPC-клиент, ввести имя хоста и адрес сервера. Настройки по умолчанию позволяют подключиться к локальному серверу;
5. За расчёт и получение, запись данных в модель отвечают два -exe файла из папки dist, для доступа к этим файлам следует поменять пути к процессам в функции calc_flowsheet()


## Ход работы

После подключения к OPC-серверу становятся доступны следующие функции:
1. Просмотр значений свойств со всех объектов модели DWSIM;
2. Просмотр значений свойств с определенного объекта с подели;
3. Редактирование значений свойств объекта с модели;
4. Настройка частоты обновления базы данных Redis (по умолчанию 5 секунд);
5. Перерасчет модели с измененными данными (по таймеру или однократно).

# Обратите внимание

1. При добавлении новых тегов в группу в программе MatrikonOpc Server for Simulation для всех объектов следует выбирать следующие настройки:

![IMAGE_DESCRIPTION](https://i.ibb.co/LvnDTjS/Setting-Object-Matrikon.png)

На текущей стадии развития проекта у всех групп(объектов) значения тегов(свойств) находятся в типе данных String, до текущего момента модули DWSIM спокойно устанавливают в модель числовые значения, даже если они считаны как строчные

2. Возможен баг со стороны DWSIM, что после проведения симуляции и сохранения модели (get_model_data.py) типы некоторых объектов со схемы начинают выводиться на португальском языке вместо английского, для таких случаев следует дополнить условный оператор `if/elif` дополнительным условием, примеры кода ниже:

```
    elif object_type == "Valve" or object_type == "Válvula":
    elif object_type == "Level Gauge" or object_type == "Medidor de Nível":
    elif object_type == "PID Controller" or object_type == "Controlador PID":
```
3. Для того, чтобы убедиться, что значения записаны на сервер можно воспользоваться MatrikonOPC Explorer, для этого необходимо подключиться к серверу "Matrikon.OPC.Simulation.1" 
Чтобы добавить теги см. Источники(пункт 5) 
В случае, если при добавлении тегов вылезает ошибка либо теги добавляются, но их значения не отображаются(как на скриншоте ниже)
![IMAGE_DESCRIPTION](https://i.ibb.co/GWdkvmw/image.png)
То нужно перейти на вкладку "Group" и снять галочку с "Use Async I/O"

В таком случае получим следующий результат:
![IMAGE_DESCRIPTION](https://i.ibb.co/9GVxr3G/2t4yw.png)

4. В коде файла set_data_to_model.py присутствуют строчки кода, похожие на те, что представлены ниже:
```
    elif object_type == "Valve" or object_type == "Válvula":
        object.SetPropertyValue('PROP_VA_0', float(current_object['CalcMode'])) #PROP_VA_0 и аналогичные далее названия свойств взяты из источников кода в гитхабе
        #можно перейти в источники со страницы API-документации либо воспользоваться поиском по репозиторию
        object.SetPropertyValue('PROP_VA_1', float(current_object['DeltaP']))
        object.SetPropertyValue('PROP_VA_2', float(current_object['OutletPressure']))
```
Как следует из названия, эта функция устанавливает значение определённого свойства объекта на указываемое, название свойств(PROP_VA_0, PROP_VA_1, PROP_VA_2) в этом и в похожих случаях не приводится ни в API-документации, ни на сайте программы – их необходимо смотреть в источниках кода на VB и C#(перейти в источники кода можно со страницы функции в API документации, нажав на кнопку "View Source")  

5. В случае, если вводимые параметры "ломают" модель DWSIM (на схеме объекты становятся красными), то просчёт схемы проводиться не будет.

6. База данных Redis(как следует из официальной документации) перед тем, как приступить к работе с ней, должна быть заранее развёрнута на компьютере
