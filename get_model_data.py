#1. Версия интерпретатора - 3.7.0, на других может не работать
#2. Версия библиотеки pythonnet - 2.x
#3. Исходные импорты лучше не трогать, может всё поломаться
#4. DWSIM желательно должна быть установлена в пути по умолчанию, настоятельно советуется при установке выбрать ВСЕ дополнительные пакеты функций
import clr
import sys
import pythoncom
import redis
import time

# Импортируем модуль System.IO, который обеспечивает доступ к операциям ввода/вывода (I/O) над файлами и каталогами
import System.IO

# Импортировать системный модуль, который обеспечивает доступ к общим типам и функциям в .NET framework.
import System

# Импортируйте модуль pythoncom, который предоставляет привязки Python для служб COM (компонентная объектная модель).
# Инициализировать COM-компоненты
pythoncom.CoInitialize()


# Импортировать классы Directory, Path и File из модуля System.IO, которые предоставляют методы для работы с каталогами и файлами
from System.IO import Directory, Path, File

# Импортировать классы String и Environment из модуля System, которые предоставляют методы для работы со строками и переменными окружения
from System import String, Environment

sys.path.append("C:\\Users\\User\Desktop\DWSIM_MODEL\dwsim_model\\venv") # добавление в sys.path пути к виртуальному окружению для корректного импорта библиотек
# Указываем путь к папке установки DWSIM
dwSimPath = r"C:\Users\User\AppData\Local\DWSIM\\"
# Добавляем ссылки на библиотеки DWSIM с помощью модуля clr
# Метод AddReference используется для добавления ссылки на сборку .NET.
# Путь к сборке строится с использованием переменной dwSimPath и имени файла сборки
# Каждая сборка предоставляет определенный набор функций
clr.AddReference(dwSimPath + "CapeOpen.dll")
clr.AddReference(dwSimPath + "DWSIM.Automation.dll")
clr.AddReference(dwSimPath + "DWSIM.Interfaces.dll")
clr.AddReference(dwSimPath + "DWSIM.GlobalSettings.dll")
clr.AddReference(dwSimPath + "DWSIM.SharedClasses.dll")
clr.AddReference(dwSimPath + "DWSIM.Thermodynamics.dll")
clr.AddReference(dwSimPath + "DWSIM.UnitOperations.dll")
clr.AddReference(dwSimPath + "DWSIM.Inspector.dll")
clr.AddReference(dwSimPath + "DWSIM.DynamicsManager.dll")
clr.AddReference(dwSimPath + "System.Buffers.dll")
clr.AddReference(dwSimPath + "DWSIM.FlowsheetSolver.dll")
# Импорт определенных классов из библиотек DWSIM с использованием синтаксиса "from...import"
# Классы используются в последующем коде для доступа к определенным функциям, предоставляемым DWSIM.
from DWSIM.Interfaces.Enums.GraphicObjects import ObjectType
from DWSIM.Thermodynamics import Streams, PropertyPackages
from DWSIM.UnitOperations import UnitOperations
from DWSIM.Automation import Automation, Automation2, Automation3
from DWSIM.GlobalSettings import Settings
from DWSIM.DynamicsManager import *
from DWSIM.FlowsheetSolver import FlowsheetSolver

# Решить что делать с единицами измерениями, через API не получается (по крайней мере сейчас)
def get_object_properties(object, object_type):
    if object_type == "Material Stream":
        component_ids = ''.join([str(id) for id in object.get_ComponentIds()])
        overall_composition = ''.join([str(compose) for compose in object.GetOverallComposition()])
        print(f"Температура: {object.GetTemperature()} K")
        print(f"Давление: {object.GetPressure()} Pa")
        print(f"Массовый поток: {object.GetMassFlow()} kg/s")
        print(f"Молярный поток: {object.GetMolarFlow()} mol/s")
        print(f"Объемный расход потока: {object.GetVolumetricFlow()} m^3/s")
        print(f"Удельная энтальпия потока: {object.GetMassEnthalpy()} kJ/kg")
        print(f"Удельная энтропия потока: {object.GetMassEntropy()} kJ/kg.K")
        print(f"Энергия потока: {object.GetEnergyFlow()} kW")
        print(f"Компоненты потока: {component_ids}")
        print(f"Массовая доля компонентов потока {overall_composition}")
        print(f"Количество фаз потока: {object.GetNumPhases()}")

        object_properties_dict = {'Temperature': object.GetTemperature(), 'Pressure': object.GetPressure(),
                                  'MassFlow': object.GetMassFlow(), 'MolarFlow': object.GetMolarFlow(),
                                  'VolumetricFlow': object.GetVolumetricFlow(), 'MassEnthalpy': object.GetMassEnthalpy(),
                                  'MassEntropy': object.GetMassEntropy(), 'EnergyFlow': object.GetEnergyFlow(),
                                  'ComponentIds': component_ids, 'OverallComposition': overall_composition,
                                  'NumPhases': object.GetNumPhases()}
        print('--------------------------------------')
        return object_properties_dict
    elif object_type == "Valve" or object_type == "Válvula":
        print(f"Выходное давление: {object.OutletPressure} Pa")
        print(f"Выходная температура: {object.OutletTemperature} K")
        print(f"Перепад давления: {object.DeltaP} Pa")
        print(f"Тип расчета: {object.CalcMode}")
        object_properties_dict = {'OutletPressure': object.OutletPressure, 'OutletTemperature': object.OutletTemperature,
                                  'DeltaP': object.DeltaP, 'CalcMode': object.CalcMode}
        print('--------------------------------------')
        return object_properties_dict
    elif object_type == "Tank":
        print(f"Объём: {object.Volume} m^3")
        print(f"Время пребывания жидкости: {object.ResidenceTime} s")
        object_properties_dict = {'Volume': object.Volume, 'ResidenceTime': object.ResidenceTime}
        print('--------------------------------------')
        return object_properties_dict
    elif object_type == "Level Gauge" or object_type == "Medidor de Nível":
        print(f"Минимальный объём: {object.MinimumValue}")
        print(f"Максимальный объём: {object.MaximumValue}")
        print(f"Текущий объём: {object.CurrentValue}")
        print(f"Свойство: {object.SelectedProperty}")
        object_properties_dict = {'MinimumValue': object.MinimumValue, 'MaximumValue': object.MaximumValue,
                                  'CurrentValue': object.CurrentValue, 'SelectedProperty': object.SelectedProperty}
        print('--------------------------------------')
        return object_properties_dict
    elif object_type == "PID Controller" or object_type == "Controlador PID":
        print(f"Заданное значение: {object.SetPoint}")
        print(f"Смещение:{object.Offset}")
        print(f"Максимальная выходная мощность: {object.OutputMax}")
        print(f"Минимальная выходная мощность: {object.OutputMin}")
        print(f"Kp: {object.Kp}")
        print(f"Ki: {object.Ki}")
        print(f"Kd: {object.Kd}")
        print(f"Заводной предохранитель: {object.WindupGuard}")
        object_properties_dict = {'SetPoint': object.SetPoint, 'Offset': object.Offset,
                                  'OutputMax': object.OutputMax, 'OutputMin': object.OutputMin,
                                  'Kp': object.Kp, 'Ki': object.Ki, 'Kd': object.Kd}
        print('--------------------------------------')
        return object_properties_dict
    return {}



r = redis.Redis(host='localhost', port=6379, decode_responses=True)

# Установите текущий каталог в папку установки DWSIM, используя класс Directory из модуля System.IO
# Это необходимо для корректного поиска и импорта библиотек DWSIM
Directory.SetCurrentDirectory(dwSimPath)
# Создайте экземпляр класса Automation3 из модуля DWSIM.Automation
# Этот класс предоставляет методы для автоматизации задач в DWSIM, таких как создание технологических карт и управление ими.
interf = Automation3()
# Установите путь к файлу существующей поточной таблицы DWSIM для загрузки с помощью метода Path.Combine из модуля System.IO
# Путь к файлу схемы создается с использованием метода Environment.GetFolderPath для получения пути к папке на рабочем столе и относительного пути к файлу схемы.
fileNameToLoad = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), r"C:\Users\User\AppData\Local\DWSIM\samples\edited_samples\Water_Level_Tank_Control_Edited.dwxmz")
# Загрузите блок-схему DWSIM с помощью метода LoadFlowsheet класса Automation3.
# Метод принимает единственный аргумент, который представляет собой путь к файлу загружаемой схемы.
# Метод возвращает объект Simulation
# По какой-то причине функция не кушает относительный путь к файлу, только абсолютный. Ошибка отсылает в исходники кода DWSIM

sim = interf.LoadFlowsheet(fileNameToLoad)
Settings.SolverMode = 0 #очень важный параметр, без него не будет осуществлено изменение параметров объектов схемы просчитанной модели
errors = interf.CalculateFlowsheet2(sim) #считается статическая схема
flowsheet_objects = sim.get_SimulationObjects()

for obj in flowsheet_objects:
    object_name = obj.get_Value().ToString()
    object_type = obj.get_Value().GetAsObject().GetDisplayName()
    try:
        object_name = object_name[:object_name.index(':')]
        print(f"Название объекта: {object_name}")
    except:
        print(f"Название объекта: {object_name}")
    print(f"Тип объекта: {object_type}")
    current_object = sim.GetFlowsheetSimulationObject(object_name)
    current_object_properties = get_object_properties(current_object, object_type)
    r.hmset(object_name, mapping=current_object_properties) #выдаёт ошибку, но отрабатывает так, как должна. Ошибка говорит о том, что функция устарела
keys = r.keys("*")
print(keys)

fileNameToSave = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), r"C:\Users\User\AppData\Local\DWSIM\samples\edited_samples\Water_Level_Tank_Control_Edited.dwxmz")
interf.SaveFlowsheet(sim, fileNameToSave, True)

time.sleep(2)

