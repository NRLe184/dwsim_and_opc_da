﻿using OPCAutomation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Threading;
using System.Windows.Shapes;
using StackExchange.Redis;
using System.Diagnostics;
using System.Text.RegularExpressions;
using static System.Net.Mime.MediaTypeNames;
using System.DirectoryServices;
using System.Collections.Concurrent;
using System.Diagnostics.Eventing.Reader;
using System.Reflection.Emit;
using System.IO;

namespace OPC_DA
{


    public partial class MainWindow : Window
    {

        OPCServer MyServer;
        OPCGroup MyGroup;
        OPCItem Myitem;
        List<OPCGroup> My_groups = new List<OPCGroup>();

        ConnectionMultiplexer redis;
        IDatabase db;
        List<string> Group_names = new List<string>();



        List<Array> ClientServerHandles = new List<Array>();
        List<Array> ClientItemHandles = new List<Array>();
        List<Array> MyValues = new List<Array>();
        List<Array> MyErrors = new List<Array>();

        List<OPCItem> Grr = new List<OPCItem>();

        double selected_slider_value;

        bool is_stop_calc = false;

        double redis_update_rate = 5;

        public class Item
        {
            public string property_name { get; set; }
            public object property_value { get; set; }
        };

        public MainWindow()
        {

            InitializeComponent();
            try
            {
                timer_value_seconds.Content = select_value_timer.Minimum;
            }
            catch (Exception ex)
            {
                redis_status.Content = ex.Message;
            }
        }

        private void connect_to_redis(string host = "localhost", string port = "6379", string allowAdmin = "true")
        {

            redis = ConnectionMultiplexer.Connect($"{host}:{port},allowAdmin={allowAdmin}");
        }

        private async Task ExecuteAsync(CancellationToken stoppingToken)
        {
 
            var subscriber = redis.GetSubscriber(stoppingToken);
            try
            {
                await Task.Run(async () =>
                {
                    while (!stoppingToken.IsCancellationRequested)
                    {
                        db = redis.GetDatabase();
                        Dispatcher.Invoke((Action)(() =>
                        {
                            redis_status.Content = db;
                        }));
                        

                        Group_names.Clear();
                        My_groups.Clear();
                        try
                        {
                            if (MyServer.ServerName != "")
                            {
                                MyServer.OPCGroups.RemoveAll();
                            }
                        }
                        catch
                        {

                        }
                        ClientItemHandles.Clear();
                        ClientServerHandles.Clear();

                        string[] keys_array = get_redis_keys();
                        add_group_names(keys_array);
                        print_group_names();
                        add_groups_and_tags_to_server(Group_names);
                        update_group_box();
                        update_select_opc_item();
                        await Task.Delay((int)(redis_update_rate * 1000), stoppingToken);
                    }
                });
                }
            catch (Exception ex)
            {

            }
        }

        private string[] get_redis_keys(string host = "localhost", string port = "6379")
        {
            var keys = redis.GetServer($"{host}", int.Parse(port)).Keys();
            return keys.Select(key => (string)key).ToArray();
        }

        private void add_group_names(string[] keys_array)
        {

            foreach (string key in keys_array)
            {
                Group_names.Add(key);
                var current_obj = db.HashGetAll(key);
                Array handle = new int[current_obj.Length + 1]; //число объектов в группе + 1 указывается в скобках, например, если 1 значение, то будет 2
                ClientServerHandles.Add(handle);

            }
            Dispatcher.Invoke((Action)(() =>
            {
                operation_status.Content = "Группы успешно добавлены";
            }));

        }

        private void print_group_names()
        {
            string flowsheet_objects_properties_to_print = "Был получен список объектов со схемы: \n";
            foreach (string name in Group_names)
            {
                var current_obj = db.HashGetAll(name);
                flowsheet_objects_properties_to_print += name + ": ";
                foreach (var value in current_obj)
                {
                    flowsheet_objects_properties_to_print += value.ToString() + "\n ";
                }
            }
            Dispatcher.Invoke((Action)(() =>
            {
                flowsheet_objects_properties.Content = flowsheet_objects_properties_to_print  + "-------\n";
            }));
        }

        private void update_group_box()
        {
            Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    var selected_item = groups_box.SelectedItem;
                    groups_box.Items.Clear();
                    groups_box.IsEnabled = true;
                    groups_box.IsEditable = true;
                    foreach (OPCGroup group in My_groups)
                    {
                        groups_box.Items.Add(group.Name);

                    }
                    groups_box.Text = "Группы(объекты)";
                    groups_box.IsEditable = false;
                    groups_box.SelectedItem = selected_item;

                }
                catch (Exception ex)
                {
                    operation_status.Content = ex.ToString();
                }
            }));

        }

        private void btn_connect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MyServer = new OPCServer();
                MyServer.Connect(server_name.Text, host_address.Text);
                My_groups.Clear();
                connection_status.Content = "Подключено";
                btn_connect.IsEnabled = false;
                btn_disconnect.IsEnabled = true;
                read_write_data_group.IsEnabled = true;
                btn_write.IsEnabled = true;
                select_opc_item.IsEnabled = true;
                connect_to_redis();
                CancellationToken stoppingtoken = new CancellationToken();
                Task task = ExecuteAsync(stoppingtoken);
                btn_calc_flowsheet.IsEnabled = true;
                btn_once_calc_flowsheet.IsEnabled = true;
            }
            catch (Exception ex)
            {
                connection_status.Content += ex.ToString();
            }

        }

        private void add_groups_and_tags_to_server(List<string> Group_names)
        {
            for (int i = 0; i < Group_names.Count; i++)
            {
                int cid;
                My_groups.Add(MyServer.OPCGroups.Add(Group_names[i]));
                My_groups[i].IsActive = true;
                My_groups[i].IsSubscribed = false; //реализовать подписку на изменения
                //My_groups[i].AsyncReadComplete += MyGroup_AssyncReadComplete;
                //My_groups[i].AsyncWriteComplete += MyGroup_AssyncWriteComplete;
                var current_object_from_flowsheet = db.HashGetAll(Group_names[i]);
                Array values = Array.CreateInstance(typeof(object), current_object_from_flowsheet.Length);
                Array OPCItemIDs = Array.CreateInstance(typeof(string), current_object_from_flowsheet.Length + 1);
                Array ClientHandles = Array.CreateInstance(typeof(Int32), current_object_from_flowsheet.Length + 1);
                for (int j = 0; j < current_object_from_flowsheet.Length; j++)
                {
                    var value = current_object_from_flowsheet[j];
                    OPCItemIDs.SetValue(Group_names[i] + '.' + value.Name.ToString(), j + 1);//Group_names[i] + '.' + value.Name.ToString(), j+1);
                    ClientHandles.SetValue(j, j + 1);
                    values.SetValue(value.Value.ToString(), j);
                    OPCItem item = My_groups[i].OPCItems.AddItem(Group_names[i].ToString() + '.' + value.Name.ToString(), j);
                    try
                    {
                        item.Write((string)value.Value.ToString());
                    }
                    catch (Exception ex)
                    {

                    }
                }
                MyValues.Add(values);
            }
        }

        private void btn_disconnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //MyGroup.IsSubscribed = false;
                //MyGroup.IsActive = false;
                MyServer.OPCGroups.RemoveAll();
                MyServer.Disconnect();
                MyServer = null;
                select_opc_item.Items.Clear();
                input_new_value.Clear();
                btn_connect.IsEnabled = true;
                btn_disconnect.IsEnabled = false;
                select_opc_item.IsEnabled = false;
                read_write_data_group.IsEnabled = false;
                btn_write.IsEnabled = false;
                
                connection_status.Content = "Отключено";
            }
            catch (Exception ex)
            {
                connection_status.Content = "Ошибка отключения " + ex.Message;
            }
        }

        private void btn_subscribe_Click(object sender, RoutedEventArgs e)
        {
            foreach (OPCGroup group in My_groups)
            {
                group.DataChange += MyGroup_DataChange;
            }

        }

        private void MyGroup_DataChange(int TransactionID, int NumItems, ref Array ClientHandles, ref Array ItemValues, ref Array Qualities, ref Array TimeStamps)
        {
            // item_value.Text = ItemValues.GetValue(1).ToString();
        }




        private void groups_box_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dataGridView.Columns.Clear();
            dataGridView.Items.Clear();


            DataGridTextColumn property_name = new DataGridTextColumn();
            property_name.Header = "Свойство объекта";
            property_name.Binding = new Binding("property_name");
            dataGridView.Columns.Add(property_name);

            DataGridTextColumn property_value = new DataGridTextColumn();
            property_value.Header = "Значение свойства";
            property_value.Binding = new Binding("property_value");
            dataGridView.Columns.Add(property_value);


            try
            {
                for (int i = 0; i < My_groups.Count; i++)
                {
                    if (groups_box.SelectedValue != null && My_groups[i].Name == groups_box.SelectedValue.ToString())
                    {
                        foreach (OPCItem item in My_groups[i].OPCItems)
                        {
        
                            dataGridView.Items.Add(new Item() { property_name = item.ItemID, property_value = item.Value });
                        }
                    }



                }
            }

            catch (Exception ex)
            {

            }

            /*
            foreach (OPCGroup group in My_groups)
            {
                if(group.Name == groups_box.SelectedValue.ToString())
                {
                    foreach(OPCItem item in group.OPCItems.GetOPCItem(MyServerHandles))
                    {
                        var a = item.AccessPath.ToString();
                        var b = item.Value;
                        dataGridView.Items.Add(new Item() { property_name = item.AccessPath, property_value = item.Value });
                    }
                }
            }*/
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void listen_redis_btn_Click(object sender, RoutedEventArgs e)
        {

            Group_names.Clear();
            My_groups.Clear();
            try
            {
                MyServer.OPCGroups.RemoveAll();
            }
            catch
            {

            }
            ClientItemHandles.Clear();
            ClientServerHandles.Clear();
            connect_to_redis();
            string[] keys_array = get_redis_keys();
            add_group_names(keys_array);
            print_group_names();
            add_groups_and_tags_to_server(Group_names);
            update_group_box();
            update_select_opc_item();
        }

        private void update_select_opc_item()
        {
            Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    var selected_opc_item = select_opc_item.SelectedItem;
                    select_opc_item.Items.Clear();
                    for (int i = 0; i < My_groups.Count; i++)
                    {
                        foreach (OPCItem item in My_groups[i].OPCItems)
                        {
                            select_opc_item.Items.Add(item.ItemID);

                        }
                    }
                    select_opc_item.SelectedItem = selected_opc_item;
                }
                catch (Exception ex)
                {
                    operation_status.Content = ex.Message;
                }
            }));
            
        }

        private void dataGridView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btn_write_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                //MyServer.OPCGroups.GetOPCGroup("V-02").OPCItems.GetEnumerator();
                string item_address = select_opc_item.SelectedItem.ToString();
                string[] group_item = item_address.Split(new char[] { '.' });
                foreach (OPCItem item in MyServer.OPCGroups.GetOPCGroup(group_item[0]).OPCItems)
                {
                    if (item.ItemID == item_address && input_new_value.Text != null)
                    {
                        item.Write(input_new_value.Text);
                        operation_status.Content = string.Format("Значение тега {0} успешно записано на сервер", item_address);
                        dataGridView.Columns.Clear();
                        dataGridView.Items.Clear();
                        db.HashSet(group_item[0], group_item[1], input_new_value.Text);
                        break;
                    }
                }


                //  input_opc_item_addres.Text;
            }
            catch (Exception ex)
            {
                operation_status.Content = string.Format("Во время записи возникла ошибка: {0}", ex);
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void redis_status_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btn_once_calc_flowsheet_Click(object sender, RoutedEventArgs e)
        {
            calc_flowsheet();
        }

        private void select_value_timer_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            selected_slider_value = Math.Round(select_value_timer.Value);
            timer_value_seconds.Content = selected_slider_value.ToString();
        }

        async private Task calc_flowsheet()
        {
            using (Process set_data_to_moodel_process = new Process())
            {
                set_data_to_moodel_process.StartInfo.UseShellExecute = false;
                string current_directory = Directory.GetCurrentDirectory();
                DirectoryInfo parent_directory = Directory.GetParent(current_directory);
                set_data_to_moodel_process.StartInfo.FileName = "C:\\Users\\User\\Desktop\\CLIENT_OPC_DA\\dwsim_and_opc_da\\dist\\set_data_to_model.exe";
                set_data_to_moodel_process.StartInfo.CreateNoWindow = false;
                set_data_to_moodel_process.Start();
                set_data_to_moodel_process.WaitForExit();
            }

            using (Process get_data_from_moodel_process = new Process())
            {
                get_data_from_moodel_process.StartInfo.UseShellExecute = false;
                get_data_from_moodel_process.StartInfo.FileName = "C:\\Users\\User\\Desktop\\DWSIM_MODEL\\dwsim_model\\dist\\get_model_data.exe";
                get_data_from_moodel_process.StartInfo.CreateNoWindow = false;
                get_data_from_moodel_process.Start();
                get_data_from_moodel_process.WaitForExit();
            }
        }

        private async Task start_calc_flowsheet_on_timer_expiration()
        {
            await Task.Run(async () =>
            {

                try
                {
                    while (true)
                    {

                        for (int i = 0; i < selected_slider_value; i++)
                        {
                            if (is_stop_calc == true)
                            {
                                is_stop_calc = false;
                                return;
                            }

                            Dispatcher.Invoke((Action)(() =>
                            {
                                timer_value_seconds.Content = (select_value_timer.Value - i).ToString();
                            }));
                            await Task.Delay(1000);
                        }
                        await calc_flowsheet();
                    }
                }
                catch (Exception ex)
                {

                }
            });
            
        }

        private void btn_calc_flowsheet_Click(object sender, RoutedEventArgs e)
        {
            btn_cancel_calc.IsEnabled = true;
            select_value_timer.IsEnabled = false;
            start_calc_flowsheet_on_timer_expiration();
        }

        private void btn_cancel_calc_Click(object sender, RoutedEventArgs e)
        {
            is_stop_calc = true;
            btn_cancel_calc.IsEnabled = false;
            select_value_timer.IsEnabled = true;
            timer_value_seconds.Content = select_value_timer.Value.ToString();
        }

        private static readonly Regex _regex = new Regex("[^0-9.-]+"); 
        private static bool IsTextAllowed(string text)
        {
            return !_regex.IsMatch(text);
        }

        private void input_update_rate_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsTextAllowed(input_update_rate.Text))
            {
                btn_change_update_rate.IsEnabled = true;
            }
            else
            {
                btn_change_update_rate.IsEnabled = false;   
            }
        }

        private void btn_change_update_rate_Click(object sender, RoutedEventArgs e)
        {
            string[] value_for_convert = input_update_rate.Text.Split(new char[] { '.' });
            try 
            {
                redis_update_rate = Convert.ToDouble(value_for_convert[0] + ',' + value_for_convert[1]);
            }
            catch
            {
                redis_update_rate = Convert.ToDouble(value_for_convert[0] + ',' + '0');
            }
            operation_status.Content = "Частота обновления базы данных redis установлена на " + input_update_rate.Text;
        }
    }
}

