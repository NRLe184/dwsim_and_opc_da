﻿#pragma checksum "..\..\..\..\MainWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "A38418901CACB348F6875A8331C5E509C74B9B69"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using OPC_DA;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace OPC_DA {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 1 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal OPC_DA.MainWindow OPC_DA_CLIENT;
        
        #line default
        #line hidden
        
        
        #line 9 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid txt;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label l;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox host_address;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label server_name_label;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox server_name;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_connect;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox read_write_data_group;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_write;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label redis_status;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label l_Copy;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label connection_status;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox input_new_value;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_disconnect;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox groups_box;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dataGridView;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label l_Copy1;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label operation_status;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button listen_redis_btn;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer flowsheet_objects_properties;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox select_opc_item;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_once_calc_flowsheet;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_calc_flowsheet;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label timer_value_seconds;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider select_value_timer;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_cancel_calc;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox redis_settings_group;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_change_update_rate;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox input_update_rate;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "7.0.8.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/OPC_DA;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "7.0.8.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.OPC_DA_CLIENT = ((OPC_DA.MainWindow)(target));
            return;
            case 2:
            this.txt = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.l = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.host_address = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.server_name_label = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.server_name = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.btn_connect = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\..\..\MainWindow.xaml"
            this.btn_connect.Click += new System.Windows.RoutedEventHandler(this.btn_connect_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.read_write_data_group = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 9:
            this.btn_write = ((System.Windows.Controls.Button)(target));
            
            #line 19 "..\..\..\..\MainWindow.xaml"
            this.btn_write.Click += new System.Windows.RoutedEventHandler(this.btn_write_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.redis_status = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.l_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.connection_status = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.input_new_value = ((System.Windows.Controls.TextBox)(target));
            
            #line 23 "..\..\..\..\MainWindow.xaml"
            this.input_new_value.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 14:
            this.btn_disconnect = ((System.Windows.Controls.Button)(target));
            
            #line 24 "..\..\..\..\MainWindow.xaml"
            this.btn_disconnect.Click += new System.Windows.RoutedEventHandler(this.btn_disconnect_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.groups_box = ((System.Windows.Controls.ComboBox)(target));
            
            #line 25 "..\..\..\..\MainWindow.xaml"
            this.groups_box.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.groups_box_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 16:
            this.dataGridView = ((System.Windows.Controls.DataGrid)(target));
            
            #line 26 "..\..\..\..\MainWindow.xaml"
            this.dataGridView.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dataGridView_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 17:
            this.l_Copy1 = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.operation_status = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.listen_redis_btn = ((System.Windows.Controls.Button)(target));
            
            #line 30 "..\..\..\..\MainWindow.xaml"
            this.listen_redis_btn.Click += new System.Windows.RoutedEventHandler(this.listen_redis_btn_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.flowsheet_objects_properties = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 21:
            this.select_opc_item = ((System.Windows.Controls.ComboBox)(target));
            
            #line 32 "..\..\..\..\MainWindow.xaml"
            this.select_opc_item.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ComboBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 22:
            this.btn_once_calc_flowsheet = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\..\..\MainWindow.xaml"
            this.btn_once_calc_flowsheet.Click += new System.Windows.RoutedEventHandler(this.btn_once_calc_flowsheet_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.btn_calc_flowsheet = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\..\..\MainWindow.xaml"
            this.btn_calc_flowsheet.Click += new System.Windows.RoutedEventHandler(this.btn_calc_flowsheet_Click);
            
            #line default
            #line hidden
            return;
            case 24:
            this.timer_value_seconds = ((System.Windows.Controls.Label)(target));
            return;
            case 25:
            this.select_value_timer = ((System.Windows.Controls.Slider)(target));
            
            #line 37 "..\..\..\..\MainWindow.xaml"
            this.select_value_timer.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.select_value_timer_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 26:
            this.btn_cancel_calc = ((System.Windows.Controls.Button)(target));
            
            #line 38 "..\..\..\..\MainWindow.xaml"
            this.btn_cancel_calc.Click += new System.Windows.RoutedEventHandler(this.btn_cancel_calc_Click);
            
            #line default
            #line hidden
            return;
            case 27:
            this.redis_settings_group = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 28:
            this.btn_change_update_rate = ((System.Windows.Controls.Button)(target));
            
            #line 40 "..\..\..\..\MainWindow.xaml"
            this.btn_change_update_rate.Click += new System.Windows.RoutedEventHandler(this.btn_change_update_rate_Click);
            
            #line default
            #line hidden
            return;
            case 29:
            this.input_update_rate = ((System.Windows.Controls.TextBox)(target));
            
            #line 43 "..\..\..\..\MainWindow.xaml"
            this.input_update_rate.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.input_update_rate_TextChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

